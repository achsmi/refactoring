<?php
/**
 * Created by PhpStorm.
 * User: acs
 * Date: 12/06/2019
 * Time: 13:46
 */

class Statement
{
    /**
     * Specific function for replace statement data
     * @param $replace
     * @param $text
     */
    public function replaceStatement($replace, &$text)
    {
        $text = str_replace(
            $replace[0],
            $replace[1],
            $text
        );
    }

}
