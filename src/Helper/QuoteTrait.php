<?php

trait QuoteTrait
{

    /**
     * Specific function for replace Quote statement data , The text could be pass by reference
     * @param $quote
     * @param $text
     */
    private function replaceQuoteStatument($quote, &$text)
    {
        list($quoteFromRepository, $usefulObject, $destination) = $this->getRepositoryData($quote);
        $replace = [[
            '[quote:summary_html]',
            '[quote:summary]',
            '[quote:destination_name]',
            '[quote:destination_link]',
        ], [
            isset($quoteFromRepository) ? Quote::renderHtml($quoteFromRepository) : "",
            isset($quoteFromRepository) ? Quote::renderText($quoteFromRepository) : "",
            isset($destination) ? $destination->countryName : "",
            (isset($usefulObject) && isset($destination) && isset($quoteFromRepository)) ? $usefulObject->url . '/' . $destination->countryName . '/quote/' . $quoteFromRepository->id : "",
        ]];
        $statement = new Statement() ;
        $statement->replaceStatement($replace, $text);
    }

    /**
     * Specific function for getting Quote statement data
     * @param $quote
     * @return array
     */
    private function getRepositoryData($quote)
    {
        return [
            QuoteRepository::getInstance()->getById($quote->id),
            SiteRepository::getInstance()->getById($quote->siteId),
            DestinationRepository::getInstance()->getById($quote->destinationId)
        ];
    }
}
