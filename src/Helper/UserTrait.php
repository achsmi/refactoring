<?php
/**
 * Created by PhpStorm.
 * User: acs
 * Date: 12/06/2019
 * Time: 11:59
 */

trait UserTrait
{


    /**
     * Specific function for replace User statement data , The text could be pass by reference
     * @param $user
     * @param $text
     */
    private function replaceUserStatument($user, &$text)
    {
        $replace = [[
            '[user:first_name]',
        ], [
            isset($user->firstname) ? ucfirst(mb_strtolower($user->firstname)) : ""
        ]
        ];
        $statement = new Statement() ;
        $statement->replaceStatement($replace, $text);    }


}
