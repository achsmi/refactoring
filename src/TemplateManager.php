<?php

class TemplateManager implements TemplateManagerInterface
{

    /**
     * @var  data treatment
     */
    private $data;

    /**
     *  separation of business logic Quote
     */
    use QuoteTrait;

    /**
     *  separation of business logic user
     */
    use UserTrait;

    /**
     * @param Template $tpl
     * @param array $data
     * @return Template
     */
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }
        $this->data = $data;
        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject);
        $replaced->content = $this->computeText($replaced->content);

        return $replaced;
    }

    /**
     * @param $text
     * @return mixed
     */
    private function computeText($text)
    {

        $data = $this->data;
        $APPLICATION_CONTEXT = ApplicationContext::getInstance();

        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;

        //Best practice to separe business logic.

        if ($quote) {
            // Specific function for quote replace statement

            $this->replaceQuoteStatument($quote, $text);
        }

        $user = (isset($data['user']) and ($data['user'] instanceof User)) ? $data['user'] : $APPLICATION_CONTEXT->getCurrentUser();
        if ($user) {

            // Specific function for user replace statement

            $this->replaceUserStatument($user, $text);
        }

        return $text;
    }


}
